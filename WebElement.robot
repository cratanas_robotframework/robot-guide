*** Settings ***
Library           Selenium2Library
Suite Setup       Open Browser               https://ngendigital.com/practice    gc
Test Setup        Maximize Browser Window
Test Teardown     Go To                      https://ngendigital.com/practice
Suite Teardown    Close Browser

*** Variable ***

*** Keywords ***

*** Test Case ***
TC01
    #ID
    Select Frame     id=iframe-015
    #Name
    Input Text       name=FirstName                      cratanas
    #Class
    Click Element    class=popup
    #CSS
    Input Text       css=#b                              33

TC02
    #ID
    Select Frame     xpath=//iframe[@id="iframe-015"]
    #Name
    Input Text       xpath=//input[@name="FirstName"]    cratanas
    #Class
    Click Element    xpath=//div[@class="popup"]




