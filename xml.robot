*** Settings ***
Library    OperatingSystem
Library    XML
Library    Collections

*** Variable ***

*** Keywords ***

*** Test Case ***
TC01
    ${XML}=                          Get File            test.xml
    ${root}=                         Parse XML	${XML}
    Should Be Equal                  ${root.tag}         example
    ${first}=                        Get Element         ${root}             first
    Should Be Equal                  ${first.text}       text
    Dictionary Should Contain Key    ${first.attrib}     id
    Element Text Should Be           ${first}            text
    Element Attribute Should Be      ${first}            id	1
    Element Attribute Should Be      ${root}             id	1	xpath=first
    Element Attribute Should Be      ${XML}              id	1	xpath=first


