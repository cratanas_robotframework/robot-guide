*** Settings ***
Library          Selenium2Library
Suite Teardown   Close All Browsers  

*** Test Case ***
Test Chrome
    Open Browser    https://www.google.com/    gc
    Close Browser

Test IE
    Open Browser    https://www.google.com/  edge
    Close Browser

Test Firefox
    Open Browser    https://www.google.com/    ff
    Close Browser