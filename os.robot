*** Settings ***
Library         OperatingSystem

*** Variable ***
${OUTPUTDIR}    D://temp


*** Keywords ***
#----- Append To Environment Variable name, *values, **config
Set Env
    Append To Environment Variable    output_dir                    D://temp
    Set Environment Variable          filename1                     test.txt
    Set Environment Variable          filename2                     test2.txt


*** Test Case ***
TC01
    Set Env
    Log To Console                    %{output_dir}

TC02
    [Documentation]                   Create File                   path, content=, encoding=UTF-8
    Create File                       %{output_dir}/%{filename1}    HelloWorld นะครับ                 UTF-8

TC03
    [Documentation]                   Copy File                     *sources_and_destination
    Copy File                         %{output_dir}/%{filename1}    %{output_dir}/%{filename2}

TC04
    [Documentation]                   Get File
    ${abc}=                           Get File                      %{output_dir}/%{filename1}        UTF-8
    Log To Console                    ${abc}
    Log To Console                    ${OUTPUTDIR}

TC05
    [Documentation]                   Remove File
    Remove File                       %{output_dir}/%{filename2}

