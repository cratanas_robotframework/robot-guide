*** Settings ***
Library           Selenium2Library
Library           ExcelRobot
Suite Setup       Open Browser               https://ngendigital.com/demo-application    gc
Test Setup        Maximize Browser Window
Test Teardown     Go To                      https://ngendigital.com/demo-application
Suite Teardown    Close Browser

*** Variable ***

*** Keywords ***
login to demo application
    [Arguments]                       ${username}                                               ${password}               ${login_expect}
    Select Frame                      iframe-015
    Input Text                        xpath://input[@name='email']                              ${username}
    input password                    xpath://input[@name='passwd']                             ${password}
    Click Element                     xpath://div[@name='SignIn']
    LOG                               ${login_expect}
    run Keyword if                    '${login_expect}'=='FOUND'                                actions_check_found
    ...                               ELSE                                                      actions_check_notfound

actions_check_found
    Select Frame                      iframe-115
    Wait Until Page Contains          Book your Flight
    Log off

actions_check_notfound
    Wait Until Page Contains          Invalid username/password

Log off
    Click Button                      xpath=//button[@class="tablinks" and text()="Log out"]

*** Test Case ***
Login1
    Open Excel                        test1.xlsx
    ${row_count}                      Get Row Count                                             Sheet1
    : FOR  ${var1}  IN RANGE  0  ${row_count}
    \    ${username}                  Read Cell Data                                            Sheet1                    0                  ${var1}
    \    ${password}                  Read Cell Data                                            Sheet1                    1                  ${var1}
    \    ${login_expect}              Read Cell Data                                            Sheet1                    2                  ${var1}
    \    login to demo application    ${username}                                               ${password}               ${login_expect}
    sleep                             3s


