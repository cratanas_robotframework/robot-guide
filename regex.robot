*** Settings ***
#temp robot

*** Variable ***

*** Keywords ***
Validate Number Only
    [Arguments]                    ${number}
    Should Match Regexp            ${number}                                                   ^[0-9]

Validate Number Range
    [Arguments]                    ${number}                                                   ${min}       ${max}
    Run Keyword Unless             ${min} < ${number} < ${max}                                 Fail

Validate String Only
    [Arguments]                    ${string}
    Should Not Match Regexp        ${string}                                                   ^[0-9]

Validate Size
    [Arguments]                    ${string}                                                   ${len}
    ${length}=                     Get Length                                                  ${string}
    Should Be Equal As Integers    ${length}	${len}

Validate Eng Only
    [Arguments]                    ${string}
    Should Match Regexp            ${string}                                                   ^[A-Za-z]

$Validate thai Only


*** Test Case ***
Validate Number Only
    Validate Number Only           123

Validate Number Range
    Validate Number Range          4.5                                                         1            5

Validate String Only
    Validate String Only           aaABvCDEFGHIsJKLMNOPQRSTUVWXYZabcdefghigklmnokqrstuvwxyz

Validate Size
    Validate Size                  abd123                                                      6

Validate Eng Only
    Validate Eng Only              abcDEf5Gx4ก
