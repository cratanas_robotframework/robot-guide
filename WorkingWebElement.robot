*** Settings ***
Library           Selenium2Library
Library           Selenium2Library
Suite Setup       Open Browser               https://ngendigital.com/practice    gc
Test Setup        Maximize Browser Window
Test Teardown     Go To                      https://ngendigital.com/practice
Suite Teardown    Close Browser

*** Variable ***

*** Keywords ***

*** Test Case ***
TC-Iframe
    Element Should Be Visible        xpath=//iframe[@id="iframe-015"]
    Select Frame                     xpath=//iframe[@id="iframe-015"]

TC-TextBox
    Select Frame                     xpath=//iframe[@id="iframe-015"]
    Input Text                       xpath=//input[@name="FirstName"]               cratanas
    Textfield Value Should Be        xpath=//input[@name="FirstName"]               cratanas

TC-Checkbox
    Select Frame                     xpath=//iframe[@id="iframe-015"]
    Select Checkbox                  xpath=//input[@name="car" and @value="por"]
    Checkbox Should Be Selected      xpath=//input[@name="car" and @value="por"]

TC-Button
    Select Frame                     xpath=//iframe[@id="iframe-015"]
    Click Button                     xpath=//input[@value="Click Me"]
    Wait Until Page Contains         Button clicked

TC-Radio
    Select Frame                     xpath=//iframe[@id="iframe-015"]
    Select Radio Button              flight                                         flighthotel
    Radio Button Should Be Set To    flight                                         flighthotel

TC-List
    Select Frame                     xpath=//iframe[@id="iframe-015"]
    Select From List By Value        xpath=//select[@id="FromCity"]                 ord
    List Selection Should Be         xpath=//select[@id="FromCity"]                 Chicago
    Select From List By Label        xpath=//select[@name="cars"]                   Saab
    List Selection Should Be         xpath=//select[@name="cars"]                   Saab
    Input Text                       xpath=//input[@name="browser"]                 Opera
    Textfield Value Should Be        xpath=//input[@name="browser"]                 Opera



TC-Table
    Select Frame                     xpath=//iframe[@id="iframe-015"]
    Table Row Should Contain         xpath=//html/body/fieldset[7]/table            2                 Justin Trudeau
    Table Column Should Contain      xpath=//html/body/fieldset[7]/table            1                 Justin Trudeau
    Table Cell Should Contain        xpath=//html/body/fieldset[7]/table            2                 1                 Justin Trudeau
    Table Should Contain             xpath=//html/body/fieldset[7]/table            Justin Trudeau

