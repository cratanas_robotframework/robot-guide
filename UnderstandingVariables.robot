*** Settings ***

*** Variable ***
${var1}     hello world
${var2}     hello world
${var3}     4.4
${var4}     4.4
${var5}     4
${var6}     5

@{list1}    ['a1','a2','a3']
&{user}     name=cratanas       topic=robot framework
&{USER1}    name=Matti          address=xxx              phone=123
&{USER2}    name=Teppo          address=yyy              phone=456

*** Keywords ***

*** Test Case ***
TC01
    Log Many                           ${var1}           ${var2}

    Should Be Equal As Strings         ${var1}           ${var2}

    Log Many                           ${var3}           ${var4}

    Should Be Equal As Numbers         ${var3}           ${var4}

    Log Many                           ${var5}           ${var6}

    Should Not Be Equal As Integers    ${var5}           ${var6}

    Log Many                           @{list1}{0}       @{list1}{1}

    Should Not Be Equal As Strings     @{list1}{0}       @{list1}{1}

    Log Many                           &{user}[name]     &{user}[topic]

    Log Many                           &{USER1}[name]    &{USER1}[address]    &{USER1}[phone]

    Log Many                           &{USER2}[name]    &{USER2}[address]    &{USER2}[phone]