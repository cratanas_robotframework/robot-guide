*** Settings ***
Library           Selenium2Library
Suite Setup       Open Browser        https://eprocurement.ktb.co.th/supregis/login.aspx    gc
Test Setup        Click Element       //input[@id="ucCapcha1_ImageButton1"]
Test Teardown     Go To               https://eprocurement.ktb.co.th/supregis/login.aspx
Suite Teardown    Close Browser

*** Test Cases ***
ไม่กรอก User Name
    Click Element              //input[@id="btnLogin"]
    Alert Should Be Present    กรุณาระบุ User Name

ไม่กรอก Password
    Input Text                 //input[@id="txtUser"]     cratanas
    Click Element              //input[@id="btnLogin"]
    Alert Should Be Present    กรุณาระบุ Password
