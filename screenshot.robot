*** Settings ***
Library    Selenium2Library
Library    Screenshot

*** Variable ***

*** Keywords ***

*** Test Case ***
TC01
    Open Browser                     http://robotframework.org/robotframework/latest/libraries/Screenshot.html                                   chrome
    Wait Until Element Is Visible    xpath=//div[@id='footer-container']//a[@href='http://robotframework.org/robotframework/#built-in-tools']
    sleep                            3s
    Take Screen Shot                 abc                                                                                                         width=800px
    [Teardown]                       Close Browser
