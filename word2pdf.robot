*** Settings ***
Library    Selenium2Library

*** Variable ***

*** Keywords ***

*** Test Case ***
Word to Pdf
    Log                              abc
    Open Browser                     https://smallpdf.com/word-to-pdf                                  chrome
    sleep                            5s
    Choose File                      id=__picker-input                                                 ${EXECDIR}${/}Database-LibraryIntro.docx
    sleep                            30s
    Wait Until Element Is Visible    xpath=//div[@class="__2r1xY __1rUJE __qEyYj __3oSJa __OTYrx"]
    Click Element                    xpath=//div[@class="__3YwX9 __2Xy2F __jQQz2 jnuirl-0 fWEGS"]/a
    #[Teardown]                       Close Browser


