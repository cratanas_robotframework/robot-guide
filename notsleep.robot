*** Settings ***
Library             Selenium2Library
Test Setup          Open Browser                                                                                                                  https://www.thaiware.com/coverpage    chrome
Test Teardown       Close Browser

*** Variable ***
${enter website}    xpath=//div[@id='container']//a[@href='https://www.thaiware.com/']/img[@alt='เข้าสู่เว็บไซต์ (Enter Website) ไทยแวร์.คอม']
${text search}      xpath=//html//input[@id='main-search-input']

*** Keywords ***
เข้าสู่เว๊บไซต์
    Click Element                    ${enter website}

*** Test Case ***
TC - Sleep 5 sec
    เข้าสู่เว๊บไซต์
    Sleep                            5s

TC - Wait Until Element Is Visible
    เข้าสู่เว๊บไซต์
    Wait Until Element Is Visible    ${text search}
