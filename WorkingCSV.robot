*** Settings ***
#https://pypi.org/project/robotframework-csvlib/
Library    CSVLib

*** Variable ***

*** Variable ***

*** Test Case ***
TC01
    ${singlelist}=    Read CSV As Single List    test.csv
    log to console    ${singlelist}

    ${list}=          read csv as list           test.csv
    log to console    ${list}

    ${dict}=          read csv as dictionary     test_dict.csv     Animal    Legs          ,
    log to console    ${dict}

    ${value}=         create list                Legs              Eyes
    ${dictWList}=     read csv as dictionary     test_dict1.csv    Animal    ${value}	,
    log to console    ${dictWList}

