*** Settings ***
Library    Selenium2Library

*** Variable ***

*** Keywords ***
Open Google Site
    Open Browser        http://www.google.com    chrome

Hello
    [Arguments]         ${name}
    Log To Console      Hello ${name}!

Calcuate Plus
    [Arguments]         ${A}                     ${B}
    ${value}=           Evaluate                 (${A} * ${B}) + 10
    [Return]            ${value}

*** Test Case ***
TC01
    Hello               cratanas
    ${ret}=             Calcuate Plus            5                     6
    Log To Console      ${ret}
    Open Google Site